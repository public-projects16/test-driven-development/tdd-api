import { inject, injectable } from "inversify";
import { Song } from "./song";

import TYPES from "server/config/types";

import { SongRepository } from "./songs.repositoy";
import _ from "lodash";

@injectable()
export class SongService {

    constructor(@inject(TYPES.SongRepository) private songRepository: SongRepository) {}

    public getAllSongs(): Song[] {
        const songs: Song[] | undefined = this.songRepository.getSongs();
        if ( songs == undefined) {
            throw new Error('File not found');
        }
        return songs;
    }

    public getSongByChain(chainToSearch: string): Song {
        const songs: Song | undefined = this.songRepository.getSongByChain(chainToSearch);
        if ( songs == undefined) {
            throw new Error('Song not found');
        }
        return songs;
    }
}