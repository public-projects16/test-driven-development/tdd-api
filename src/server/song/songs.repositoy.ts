import { injectable } from "inversify";
import fs from "fs";
import * as _ from "lodash";

import { ISong } from "./song.interface";
import { Song } from "./song";

@injectable()
export class SongRepository {

    public getSongs(): Song[] | undefined {
        let rowData: Buffer;
        let jsonArray: ISong[];
        let songs: Song[] = [];
        try {
            rowData = fs.readFileSync(process.cwd() + "/songs.json");
            jsonArray =  JSON.parse(rowData.toString());
            for (const songFound of jsonArray) {
                songs.push(new Song(songFound));
            }
            return songs;
        } catch (error) {
            return undefined;
        }
    }

    public getSongByChain(chain: string): Song | undefined {
        let rowData: Buffer;
        let jsonArray: ISong[];
        try {
            rowData = fs.readFileSync(process.cwd() + "/songs.json");
            jsonArray =  JSON.parse(rowData.toString());
            const foundSong = _.find(jsonArray, (song: Song) => { return _.isEqual(song.chain, chain); }) as Song;
            return new Song(foundSong);
        } catch (error) {
            return undefined;
        }
    }

}