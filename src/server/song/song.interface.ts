export interface ISong {

    name: string;
    url: string;
    chain: string;
    id: string;
}
