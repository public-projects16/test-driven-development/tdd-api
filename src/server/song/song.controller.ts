import { Request, Response } from "express";
import { bindDependencies } from "server/config/inversify-bind";
import { Song } from "./song";
import { SongService } from "./song.service";

let injectedServices = (
    songService?: SongService
) => {
    return { songService };
};

export class SongController {

    public static instanceServices(services: Symbol[]) {
        injectedServices = bindDependencies(injectedServices, services);
    }

    public static getSong(request: Request, response: Response) {
        response.send();
    }

    public static getSongByChain(request: Request, response: Response) {
        try {
            const chain: string = request.params.id;
            const songFound: Song | undefined = injectedServices().songService?.getSongByChain(chain);
            if (songFound) { response.json(songFound); }
        } catch (error) {
            response.status(500).json(error);
        }
    }
}
