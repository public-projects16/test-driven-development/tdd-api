import { ISong } from "./song.interface";

export class Song {

    private _name: string;
    private _url: string;
    private _chain: string;
    private _id: string;

    constructor(interfacee: ISong) {
        this._name = interfacee.name;
        this._url = interfacee.url;
        this._chain = interfacee.chain;
        this._id = interfacee.id;
    }

    get name(): string { return this._name; }

    set name(value: string) { this._name = value; }

    get url(): string { return this._url; }

    set url(value: string) { this._url = value; }

    get chain(): string { return this._chain; }

    set chain(value: string) { this._chain = value; }

    get id(): string { return this._id; }

    set id(value: string) { this._id = value; }

    public toJSON() {
        return {
            name: this.name,
            url: this.url,
            chain: this.chain,
            id: this._id
        }
    }
}