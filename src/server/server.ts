import cors from "cors";
import dotenv from "dotenv";
import express, {Express} from "express";
import morgan from "morgan";
import controllers from "./config/controllers";

import apiRoutes from "./routes";

dotenv.config();

export class Server {

    private _app: Express;

    constructor() {
        this._app = express();
        this.configure();
        this.setRoutes();
    }

    public listen() {
        this.app.listen(this.app.get("port"), () => {
            console.log(`\nAPI listening in port ${ this.app.get("port") }`);
        });
    }

    private configure(): void {
        controllers();
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(morgan("dev"));
        this.app.use(cors({origin: true}));
        this.app.use(express.json());
    }

    private setRoutes(): void {
        this.app.use("/", apiRoutes);
    }

    get app(): Express { return this._app; }
}