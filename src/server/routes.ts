import { Router } from "express";

import { HomeController } from "server/home/home.controller";
import { SongController } from "./song/song.controller";

class Routes {

    private _router: Router;

    constructor() {
        this._router = Router();
        this.setRoutes();
    }

    private setRoutes(): void {
        /** HOME ROUTES  */
        this.router.get('/', HomeController.index);

        /** Song routes */

        this.router.get('/song', SongController.getSong);
        this.router.get('/song/:id', SongController.getSongByChain);
    }

    get router(): Router { return this._router; }
}

const apiRoutes = new Routes();
export default apiRoutes.router;