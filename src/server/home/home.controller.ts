import { Request, Response } from "express";

export class HomeController {

    public static index(request: Request, response: Response) {
        response.json({message: 'Works'});
    }
}