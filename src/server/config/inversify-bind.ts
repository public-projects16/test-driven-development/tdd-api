import { container } from "./inversify.config";

function bindDependencies(func: any, dependencies: any) {
    const injections = dependencies.map((dependency: any) => {
        return container.get(dependency);
    });
    return func.bind(func, ...injections);
}

function unbindDependencies(func: any, dependencies: any) {
    const injections = dependencies.map((dependency: any) => {
        return container.get(dependency);
    });
    return func.unbind(func, ...injections);
}
export { bindDependencies, unbindDependencies };