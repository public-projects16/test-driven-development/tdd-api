import { Container } from "inversify";
import "reflect-metadata";

import { SongService } from "server/song/song.service";
import { SongRepository } from "server/song/songs.repositoy";

import TYPES from "./types";

const container = new Container();

/** Services */

container.bind<SongService>(TYPES.SongService).to(SongService);

/** Repository */

container.bind<SongRepository>(TYPES.SongRepository).to(SongRepository);

export { container };