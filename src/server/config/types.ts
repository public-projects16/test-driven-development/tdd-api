const TYPES = {
    SongService: Symbol.for('SongService'),
    SongController: Symbol.for("SongController"),
    SongRepository: Symbol.for("SongRepository")
}

export default TYPES;
