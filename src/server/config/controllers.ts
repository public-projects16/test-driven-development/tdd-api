import { SongController } from "server/song/song.controller";
import TYPES from "./types";

const controllers = (): any => {
    SongController.instanceServices([TYPES.SongService]);
};

export default controllers;