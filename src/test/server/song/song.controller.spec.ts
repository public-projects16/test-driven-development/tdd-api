import { expect } from "chai";
import {Express} from "express";
import request from "supertest";

import { Server } from "server/server";
import { Song } from "server/song/song";

describe('Song controller test', () => {

    let app: Express;

    beforeEach(() => {
        app = new Server().app;
    });

    it('Song route must have status 200', (done) => {
        request(app).get('/song/00101').expect(200, (err, value) => {
            const song: Song = new Song(value.body);
            expect(song).instanceOf(Song);
            expect(song.name).equal("Un peso");
            expect(song.url).equal("https://www.youtube.com/watch?v=7o2_OnTFmmA");
            expect(song.chain).equal("00101");
            done();
        });
    });

    it("Song route must have status 500", (done) => {
        request(app).get('/song/11111').expect(500, done());
    });
});