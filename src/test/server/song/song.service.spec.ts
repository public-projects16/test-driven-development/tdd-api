import { expect } from "chai";
import { Song } from "server/song/song";
import { SongService } from "server/song/song.service";
import { SongRepository } from "server/song/songs.repositoy";

describe("Song service test", () => {

    let songRepository: SongRepository;
    let songServiceMock: SongService;

    beforeEach(() => {
        songRepository = new SongRepository();
        songServiceMock = new SongService(songRepository);
    });

    it('Validate get all songs', () => {
        let allSongs: Song[];
        try {
            allSongs = songServiceMock.getAllSongs();
            expect(allSongs[0]).instanceOf(Song);
            expect(allSongs[0].chain).equal("00000");
        } catch (error) {
            expect(error.message).equal("File not found");
        }
    });

    it("Validate get one song correctly", () => {
        const mySong: Song = songServiceMock.getSongByChain("10101");
        expect(mySong).instanceOf(Song);
        expect(mySong.name).equal("Soy peor");
        expect(mySong.chain).equal("10101");
        expect(mySong.url).equal("https://www.youtube.com/watch?v=ws00k_lIQ9U");
    });

    it("Validate get one song error", () => {
        try {
            songServiceMock.getSongByChain("11111");
        } catch(error) {
            expect(error.message).equal("Song not found");
        }
    });
});