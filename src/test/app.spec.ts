import { Server } from "server/server";
import {Express} from "express";
import request from "supertest";
import { expect } from "chai";

describe('The server should be up', () => {

    let app: Express;

    beforeEach(() => {
        app = new Server().app;
    });

    it('Server up successfully', (done) => {
        request(app).get('/').expect(200, (err, data) => {
            expect(JSON.parse(data.text).message).equal("Works");
            done();
        });
    });
});